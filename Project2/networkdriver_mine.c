/* Eric Schultz
 * 4/25/2015
 * CIS 415 Project 2
 * Network Driver source file
 */

#include "networkdriver.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

void* packet_sender();
void* packet_receiver();

boundedBuffer* sendBuf;
boundedBuffer* recBuf;
NetworkDevice* networkDevice;
FreePacketDescriptorStore* fpds;

#define BUFFER_SIZE 10

void blocking_send_packet(PacketDescriptor *pd) {

	blockingWriteBB(sendBuf, pd);
	return;

}

int  nonblocking_send_packet(PacketDescriptor *pd) {

	return nonblockingWriteBB(sendBuf, pd);

}

/* These calls hand in a PacketDescriptor for dispatching 
 * The nonblocking call must return promptly, indicating whether or 
 * not the indicated packet has been accepted by your code          
 * (it might not be if your internal buffer is full) 1=OK, 0=not OK 
 * The blocking call will usually return promptly, but there may be 
 * a delay while it waits for space in your buffers.                
 * Neither call should delay until the packet is actually sent      
 */

void blocking_get_packet(PacketDescriptor **pd, PID pid) {
	
	*pd = blockingReadBB(recBuf[pid]);
	return;

}

int  nonblocking_get_packet(PacketDescriptor **pd, PID pid) {

	return (nonblockingReadBB(recBuf[pid], (void**)pd));

}

/* These represent requests for packets by the application threads */
/* The nonblocking call must return promptly, with the result 1 if */
/* a packet was found (and the first argument set accordingly) or  */
/* 0 if no packet was waiting.                                     */
/* The blocking call only returns when a packet has been received  */
/* for the indicated process, and the first arg points at it.      */
/* Both calls indicate their process number and should only be     */
/* given appropriate packets. You may use a small bounded buffer   */
/* to hold packets that haven't yet been collected by a process,   */
/* but are also allowed to discard extra packets if at least one   */
/* is waiting uncollected for the same PID. i.e. applications must */
/* collect their packets reasonably promptly, or risk packet loss. */

void init_network_driver(NetworkDevice               *nd, 
                         void                        *mem_start, 
                         unsigned long                mem_length,
                         FreePacketDescriptorStore  **fpds_ptr) {

	int packNum = 0;

	// create pthreads
	pthread_t PacketSender; 
	pthread_t PacketReceiver;

	// create and populate fpds
	*fpds_ptr = create_fpds();
	fpds = *fpds_ptr;
	packNum = create_free_packet_descriptors(fpds, mem_start, mem_length);

	// allocate space for buffers
	sendBuf = createBB(BUFFER_SIZE);
	recBuf = createBB(BUFFER_SIZE);

	networkDevice = nd;

	// create and begin execution of threads that handle sending and receiving packets
	if( pthread_create(&PacketSender, NULL, &(packet_sender), NULL ) != 0) {
		printf("Failed to create PacketSender, exiting\n");
		exit(1);
	}

	if( pthread_create(&PacketReceiver, NULL, &(packet_receiver), NULL ) != 0) {
		printf("Failed to create PacketReceiver, exiting\n");
		exit(1);
	} 

}

void* packet_sender() {

	int i = 0;
	int sleepTime = 2;
	PacketDescriptor* pd;

	while ( 1 ) {

		pd = blockingReadBB(sendBuf);

		for (i = 0; i < 10; ++i) {
			if ( (send_packet(netdev, pd)) == 1 ) {
				break;			
			}

			sleep(sleepTime);
		}
		
		blocking_put_pd(fpds, pd);
	}
	
	return NULL;
}

void* packet_receiver() {

	PacketDescriptor *p1, *p2;
	PID pid;

	blocking_get_pd(fpds, &p1);
	blocking_get_pd(fpds, &p2);
	init_packet_descriptor(p1);
	register_receiving_packetdescriptor(networkDevice, p1);
	await_incoming_packet(networkDevice);

	while ( 1 ) {

		if (nonblockingWriteBB(buffer, p1) == 1) {

			if(nonblocking_get_pd(fpds, &p1) == 0) {

				printf("No packet descriptors available, packet lost!\n");

				init_packet_descriptor(p2);
				register_receiving_packetdescriptor(netWorkDevice, p2);

				while(nonblocking_get_pd(fpds, &p1) == 0) {
					await_incoming_packet(networkDevice);
				}

			}

			init_packet_descriptor(p1);
			register_receiving_packetdescriptor(networkDevice, p1);
			await_incoming_packet(netWorkDevice);
		}

	}

	return NULL;
}

/* Called before any other methods, to allow you to initialise */
/* data structures and start any internal threads.             */ 
/* Arguments:                                                  */
/*   nd: the NetworkDevice that you must drive,                */
/*   mem_start, mem_length: some memory for PacketDescriptors  */
/*   fpds_ptr: You hand back a FreePacketDescriptorStore into  */
/*             which you have put the divided up memory        */
/* Hint: just divide the memory up into pieces of the right size */
/*       passing in pointers to each of them                     */ 
