/* Eric Schultz
 * 4/25/2015
 * CIS 415 Project 2
 * Network Driver source file
 * This is my own work, I did however draw ideas from examples online
 */

#include "networkdriver.h"
#include "BoundedBuffer.h"
#include "packetdescriptor.h"
#include "freepacketdescriptorstore.h"
#include "freepacketdescriptorstore__full.h"
#include "packetdescriptorcreator.h"
#include "diagnostics.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void* packet_sender();
void* packet_receiver();
void* buffer_handler();

BoundedBuffer* extra;
BoundedBuffer* sendBuf;
BoundedBuffer** recBuf;
NetworkDevice* networkDevice;
FreePacketDescriptorStore* fpds;

#define BUFFER_SIZE 6

void blocking_send_packet(PacketDescriptor *pd) {

	blockingWriteBB(sendBuf, pd);
	return;

}

int  nonblocking_send_packet(PacketDescriptor *pd) {

	return nonblockingWriteBB(sendBuf, pd);

}

void blocking_get_packet(PacketDescriptor **pd, PID pid) {
	
	*pd = blockingReadBB(recBuf[pid]);
	return;

}

int  nonblocking_get_packet(PacketDescriptor **pd, PID pid) {

	return (nonblockingReadBB(recBuf[pid], (void**)pd));

}

void init_network_driver(NetworkDevice               *nd, 
                         void                        *mem_start, 
                         unsigned long                mem_length,
                         FreePacketDescriptorStore  **fpds_ptr) {

	int i = 0;

	/* Create pthreads */
	pthread_t PacketSender; 
	pthread_t PacketReceiver;
	pthread_t BufferHandler;

	/* Create and populate fpds */
	*fpds_ptr = create_fpds();
	fpds = *fpds_ptr;
	create_free_packet_descriptors(fpds, mem_start, mem_length);

	/* Allocate space for buffers */
	sendBuf = createBB(BUFFER_SIZE);
	extra = createBB(BUFFER_SIZE * 3);
	recBuf = (BoundedBuffer**)malloc((MAX_PID + 1) * (sizeof(BoundedBuffer*)));
	for(i = 0; i < MAX_PID + 1; ++i) {
		recBuf[i] = createBB(BUFFER_SIZE);
	}

	networkDevice = nd;

	/* Create and begin execution of threads that handle sending and receiving packets */
	if( pthread_create(&PacketSender, NULL, &(packet_sender), NULL) != 0 ) {
		printf("Failed to create PacketSender, exiting\n");
		exit(1);
	}

	if( pthread_create(&PacketReceiver, NULL, &(packet_receiver), NULL) != 0 ) {
		printf("Failed to create PacketReceiver, exiting\n");
		exit(1);
	} 

	if( pthread_create(&BufferHandler, NULL, buffer_handler, NULL) != 0) {
		printf("Failed to create BufferHandler, exiting\n");
		exit(1);
	}

}

/* Function executed by a thread that constantly attempts to send packets to the network device */
void* packet_sender() {

	int i = 0;
	int sleepTime = 0;
	PacketDescriptor* pd;

	while ( 1 ) {

		pd = blockingReadBB(sendBuf);

		/* Try several times to send a packet to the device, stop if successful */
		for (i = 0; i < 8; ++i) {
			if ( (send_packet(networkDevice, pd)) == 1 ) {
				break;			
			}

			sleepTime = i * i;
			usleep(sleepTime);
		}
		
		blocking_put_pd(fpds, pd);
	}
	
	return NULL;
}

/* Fuction executed by a thread that constantly checks for packets being sent to the device */
void* packet_receiver() {

	PacketDescriptor *p1, *p2;

	blocking_get_pd(fpds, &p1);
	blocking_get_pd(fpds, &p2);
	init_packet_descriptor(p1);
	register_receiving_packetdescriptor(networkDevice, p1);
	await_incoming_packet(networkDevice);

	while ( 1 ) {

		if (nonblockingWriteBB(extra, p1) == 1) {

			if(nonblocking_get_pd(fpds, &p1) == 0) {

				printf("No packet descriptors available, packet lost!\n");

				init_packet_descriptor(p2);
				register_receiving_packetdescriptor(networkDevice, p2);

				while(nonblocking_get_pd(fpds, &p1) == 0) {
					await_incoming_packet(networkDevice);
				}

			}

			init_packet_descriptor(p1);
			register_receiving_packetdescriptor(networkDevice, p1);
			await_incoming_packet(networkDevice);
		}

	}

	return NULL;
}

/* Function executed by a thread that constantly copies information */
/* from the overflow buffer into the receiver buffer */
void* buffer_handler() {

	PID pid;
	PacketDescriptor* pd;

	while( 1 ){

		pd = blockingReadBB(extra);
		pid = packet_descriptor_get_pid(pd);
		blockingWriteBB(recBuf[pid], pd);

	}

	return NULL;

}
