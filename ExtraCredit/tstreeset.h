/* Eric Schultz
 * CIS 415 Operating Systems
 * 6/5/15
 * ExtraCredit Project
 */

#ifndef _TSTREESET_H_
#define _TSTREESET_H_

#include "treeset.h"
#include "tsiterator.h"
#include <stdlib.h>
#include <pthread.h>

typedef struct tstreeset TSTreeSet;

TSTreeSet *tsts_create(int (*cmpFunction)(void *, void *));

void tsts_destroy(TSTreeSet *ts, void (*userFunction)(void *element));

int tsts_add(TSTreeSet *ts, void *element);

TSIterator *tsts_it_create(TSTreeSet *ts);

void tsts_lock(TSTreeSet* ts);

void tsts_unlock(TSTreeSet* ts);

#endif /* _TSTREESET_H_ */
