/* Eric Schultz
 * CIS 415 Operating Systems
 * 6/5/15
 * ExtraCredit Project
 */

#ifndef _TSITERATOR_H_
#define _TSITERATOR_H_

#include <stdlib.h>
#include <pthread.h>

typedef struct tsiterator TSIterator;

TSIterator *tsit_create(pthread_mutex_t* lock, long size, void **elements);

int tsit_hasNext(TSIterator *it);

int tsit_next(TSIterator *it, void **element);

void tsit_destroy(TSIterator *it);

#endif /* _TSITERATOR_H_ */
