/* Eric Schultz
 * CIS 415 Operating Systems
 * 6/5/15
 * ExtraCredit Project
 */

#ifndef _TSLINKEDLIST_H_
#define _TSLINKEDLIST_H_

#include "linkedlist.h"
#include <pthread.h>
#include <stdlib.h>

typedef struct tslinkedlist TSLinkedList;

TSLinkedList *tsll_create(void);

void tsll_destroy(TSLinkedList *ll, void (*userFunction)(void *element));

int tsll_addLast(TSLinkedList *ll, void *element);

int tsll_removeFirst(TSLinkedList* ll, void** element);

void tsll_lock(TSLinkedList* ll);

void tsll_unlock(TSLinkedList* ll);

#endif /* _TSLINKEDLIST_H_ */
