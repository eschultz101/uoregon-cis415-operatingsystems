/* Eric Schultz
 * CIS 415 Operating Systems
 * 6/5/15
 * ExtraCredit Project
 */

#include "tstreeset.h"

#define LOCK(ts) &((ts)->lock)

struct tstreeset {
   TreeSet* ts;
   pthread_mutex_t lock;
};

typedef struct tnode {
   struct tnode *link[2];
   void *element;
   int balance;
} TNode;

struct treeset {
   long size;
   TNode *root;
   int (*cmp)(void *, void *);
};

typedef struct popstruct {
   void **a;
   long len;
} PopStruct;

TSTreeSet *tsts_create(int (*cmpFunction)(void *, void *)) {

   TSTreeSet *tsts = (TSTreeSet *)malloc(sizeof(TSTreeSet));

   if (tsts != NULL) {
      TreeSet* ts = ts_create(cmpFunction);

      if (ts == NULL) {
         free(tsts);
         tsts = NULL;
      } else {
         pthread_mutexattr_t ma;
         pthread_mutexattr_init(&ma);
         pthread_mutexattr_settype(&ma, PTHREAD_MUTEX_RECURSIVE);
         tsts->ts = ts;
         pthread_mutex_init(&((tsts)->lock), &ma);
         pthread_mutexattr_destroy(&ma);
      }

   }

   return tsts;
}

void tsts_destroy(TSTreeSet *ts, void (*userFunction)(void *element)) {

   pthread_mutex_lock(LOCK(ts));
   ts_destroy(ts->ts, userFunction);
   pthread_mutex_unlock(LOCK(ts));
   pthread_mutex_destroy(LOCK(ts));
   free(ts);

}

int tsts_add(TSTreeSet *ts, void *element) {

   int result;

   pthread_mutex_lock(LOCK(ts));
   result = ts_add(ts->ts, element);
   pthread_mutex_unlock(LOCK(ts));

   return result;
}

static void populate(PopStruct *ps, TNode *node) {

   if (node != NULL) {
      populate(ps, node->link[0]);
      (ps->a)[ps->len++] = node->element;
      populate(ps, node->link[1]);
   }

}

static void **genArray(TreeSet *ts) {

   void **tmp = NULL;
   PopStruct ps;

   if (ts->size > 0L) {
      size_t nbytes = ts->size * sizeof(void *);
      tmp = (void **)malloc(nbytes);
      if (tmp != NULL) {
         ps.a = tmp;
         ps.len = 0;
         populate(&ps, ts->root);
      }
   }

   return tmp;
}

TSIterator *tsts_it_create(TSTreeSet *ts) {

   TSIterator *it = NULL;
   void **tmp = genArray(ts->ts);

   if (tmp != NULL) {
      it = tsit_create(LOCK(ts), ts->ts->size, tmp);
      if (it == NULL)
         free(tmp);
   }
   
   if(it == NULL)
      pthread_mutex_unlock(LOCK(ts));

   return it;
}

void tsts_lock(TSTreeSet* ts) {
   pthread_mutex_lock(LOCK(ts));
}

void tsts_unlock(TSTreeSet* ts) {
   pthread_mutex_unlock(LOCK(ts));
}
