/* Eric Schultz
 * CIS 415 Operating Systems
 * 6/5/15
 * ExtraCredit Project
 */

#include "tslinkedlist.h"

#define LOCK(ll) &((ll)->lock)

struct tslinkedlist {
   LinkedList *ll;
   pthread_mutex_t lock;
};

TSLinkedList *tsll_create(void) {
   TSLinkedList *tsll = malloc(sizeof(TSLinkedList));
   
   if (tsll != NULL) {
      LinkedList* ll = ll_create();
   
      if(ll == NULL) {
         free(tsll);
         tsll = NULL;
      } else {
         pthread_mutexattr_t ma;
         pthread_mutexattr_init(&ma);
         pthread_mutexattr_settype(&ma, PTHREAD_MUTEX_RECURSIVE);
         tsll->ll = ll;
         pthread_mutex_init(LOCK(tsll), &ma);
         pthread_mutexattr_destroy(&ma);
      }

   }

   return tsll;
}

void tsll_destroy(TSLinkedList *ll, void (*userFunction)(void *element)) {
   pthread_mutex_lock(LOCK(ll));
   ll_destroy(ll->ll, userFunction);
   pthread_mutex_unlock(LOCK(ll));
   pthread_mutex_destroy(LOCK(ll));
   free(ll);
}

int tsll_addLast(TSLinkedList *ll, void *element) {
   int result;

   pthread_mutex_lock(LOCK(ll));
   result = ll_addLast(ll->ll, element);
   pthread_mutex_unlock(LOCK(ll));

   return result;
}

int tsll_removeFirst(TSLinkedList *ll, void **element) {
   int result;

   pthread_mutex_lock(LOCK(ll));
   result = ll_removeFirst(ll->ll, element);
   pthread_mutex_unlock(LOCK(ll));

   return result;
}

void tsll_lock(TSLinkedList* ll) {
   pthread_mutex_lock(LOCK(ll));
}

void tsll_unlock(TSLinkedList* ll) {
   pthread_mutex_unlock(LOCK(ll));
}
