/*  Eric Schultz 
	6/1/2015
	CIS 415
	Extra Credit Project

	Starting with fileCrawlerSingle.c, you should move to a version in which there is 
	a single worker thread concurrently retrieving directories from the 
	Work Queue and placing data into the Another Structure. This will require 
	thread-safe data structures, that you determine an efficient way 
	for the worker thread to determine when there are no more 
	directories to be added to the Work Queue, and for the 
	main thread to determine when the worker thread has finished, 
	so that it can harvest the data in the Another Structure. 

	The tricky part will be how the worker threads determine that there are no 
	more directories to be added to the Work Queue, 
	and for the main thread to determine that all of the worker threads have 
	finished (without busy waiting) so it can harvest the information.

*/

#include <pthread.h>
#include <sys/types.h>
#include <dirent.h>
#include <regex.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <string.h>
#include "tslinkedlist.h"
#include "tstreeset.h"
#include "re.h"

#define _BSD_SOURCE 1

void* crawl();
int threadCreator();

/* create global variables, necessary because of multi-threading */
int numThreads = 2;
TSLinkedList *ll = NULL;
TSTreeSet *ts = NULL;
char *sp;
RegExp *reg;
int numDirs;
char** dirNames;

static void cvtPattern(char pattern[], const char *bashpat) {
   char *p = pattern;

   *p++ = '^';
   while (*bashpat != '\0') {
      switch (*bashpat) {
      case '*':
         *p++ = '.';
	 *p++ = '*';
	 break;
      case '.':
         *p++ = '\\';
	 *p++ = '.';
	 break;
      case '?':
         *p++ = '.';
	 break;
      default:
         *p++ = *bashpat;
      }
      bashpat++;
   }
   *p++ = '$';
   *p = '\0';

}

static int processDirectory(char *dirname, TSLinkedList *ll, int verbose) {
   DIR *dd;
   struct dirent *dent;
   char *sp;
   int len, status = 1;
   char d[4096];

   /* eliminate trailing slash, if there */
   strcpy(d, dirname);
   len = strlen(d);
   if (len > 1 && d[len-1] == '/')
      d[len-1] = '\0';
   
   /* open the directory */
   if ((dd = opendir(d)) == NULL) {
      if (verbose)
         fprintf(stderr, "Error opening directory `%s'\n", d);
      return 1;
   }

   /* duplicate directory name to insert into linked list */
   sp = strdup(d);
   if (sp == NULL) {
      fprintf(stderr, "Error adding `%s' to linked list\n", d);
      status = 0;
      goto cleanup;
   }
   if (!tsll_addLast(ll, sp)) {
      fprintf(stderr, "Error adding `%s' to linked list\n", sp);
      free(sp);
      status = 0;
      goto cleanup;
   }
   if (len == 1 && d[0] == '/')
      d[0] = '\0';

   /* read entries from the directory */
   while (status && (dent = readdir(dd)) != NULL) {
      if (strcmp(".", dent->d_name) == 0 || strcmp("..", dent->d_name) == 0)
         continue;
      if (dent->d_type & DT_DIR) {
         char b[4096];
         sprintf(b, "%s/%s", d, dent->d_name);
	 status = processDirectory(b, ll, 0);	/* Don't do this? */
      }
   }
cleanup:
   (void) closedir(dd);
   return status;
}

static int scmp(void *a, void *b) {
   return strcmp((char *)a, (char *)b);
}

static int applyRe(char *dir, RegExp *reg, TSTreeSet *ts) {
   DIR *dd;
   struct dirent *dent;
   int status = 1;

   /* open the directory */
   if ((dd = opendir(dir)) == NULL) {
      fprintf(stderr, "Error opening directory `%s'\n", dir);
      return 0;
   }

   /* for each entry in the directory */
   while (status && (dent = readdir(dd)) != NULL) {
      if (strcmp(".", dent->d_name) == 0 || strcmp("..", dent->d_name) == 0)
         continue;
      if (!(dent->d_type & DT_DIR)) {
         char b[4096], *sp;

	 /* see if filename matches regular expression */
	 if (! re_match(reg, dent->d_name))
            continue;
         sprintf(b, "%s/%s", dir, dent->d_name);

	 /* duplicate fully qualified pathname for insertion into treeset*/
	 if ((sp = strdup(b)) != NULL) {
        if (!tsts_add(ts, sp)) {
               fprintf(stderr, "Error adding `%s' to tree set\n", sp);
	       free(sp);
	       status = 0;
	       break;
	    }
	 } else {
           fprintf(stderr, "Error adding `%s' to tree set\n", b);
	    status = 0;
	    break;
	 }
      }
   }
   (void) closedir(dd);
   return status;
}

void* crawl() {

	while ( 1 ) {

	    if (numDirs == 3) {
	      	if (! processDirectory(".", ll, 1))
	        	break;
	    } else {
	        int i;
	        for (i = 3; i < numDirs; i++) {
	         	if (! processDirectory(dirNames[i], ll, 1))
	            	break;
	        }
	    }

		while (tsll_removeFirst(ll, (void **)&sp)) {

	      	int stat = applyRe(sp, reg, ts);
	      	free(sp);
	      	if (! stat)
	         	break;
	    }

	}

	return NULL;
}

int threadCreator() {

	pthread_t* worker;
	int i = 0;

	worker = (pthread_t*)malloc(numThreads * sizeof(pthread_t));

	for(i = 0; i < numThreads; ++i) {
		if(pthread_create(&(worker[i]), NULL, &crawl, NULL))
			return 0;
	}

	return 1;
}

int main(int argc, char *argv[]) {

   char* value;
   char pattern[4096];
   TSIterator *it;

   numDirs = argc;
   dirNames = argv;

   if (argc < 3) {
      fprintf(stderr, "Usage: ./fileCrawler ENV_VAR_NAME pattern [dir] ...\n");
      return -1;
   }

   /* get environment variable NUMTHREADS */ 
   if ((value = getenv(argv[1])) != NULL) {
	  printf("%s=%s\n", argv[1], value);
	  numThreads = atoi(value);
   } else printf("%s is not assigned.\n", argv[1]);

   /* convert bash expression to regular expression and compile */
   cvtPattern(pattern, argv[2]);
   if ((reg = re_create()) == NULL) {
      fprintf(stderr, "Error creating Regular Expression Instance\n");
      return -1;
   }
   if (! re_compile(reg, pattern)) {
      char eb[4096];
      re_status(reg, eb, sizeof eb);
      fprintf(stderr, "Compile error - pattern: `%s', error message: `%s'\n",
              pattern, eb);
      re_destroy(reg);
      return -1;
   }

   /* create linked list and treeset */
   if ((ll = tsll_create()) == NULL) {
      fprintf(stderr, "Unable to create linked list\n");
      goto done;
   }
   if ((ts = tsts_create(scmp)) == NULL) {
      fprintf(stderr, "Unable to create tree set\n");
      goto done;
   }

   /* populate linked list */
   /* for each directory in the linked list, apply regular expression */
   threadCreator();

   /* create iterator to traverse files matching pattern in sorted order */
   tsts_unlock(ts);
   if ((it = tsts_it_create(ts)) == NULL) {
      fprintf(stderr, "Unable to create iterator over tree set\n");
      goto done;
   }
   while (tsit_hasNext(it)) {
      char *s;
      (void) tsit_next(it, (void **)&s);
      printf("%s\n", s);
   }
   tsit_destroy(it);

/* cleanup after ourselves so there are no memory leaks */
done:
   if (ll != NULL)
      tsll_destroy(ll, free);
   if (ts != NULL)
      tsts_destroy(ts, free);
   re_destroy(reg);

   return 0;
}
