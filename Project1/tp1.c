// Eric Schultz test program for USPS

#include <stdio.h>
#include <unistd.h>

int main(void) {

	int i = 0;

	while (i < 10) {
	    setvbuf(stdout, NULL, _IONBF, 0);
	    printf("%d\n", i);
	    sleep(1);
	    i++;
	}

	return 0;
}
