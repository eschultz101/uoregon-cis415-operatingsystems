// Eric Schultz
// CIS 415
// Project 1: USPS Version 2
// 4/30/15
// This is my own work.

#include "p1fxns.h"
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

int main(void) {

	int pid[128] = { 0 };
	char* program[128] = { "\0" };
	char* args[128] = { "\0" };
	char buf[1024] = "\0", word[64] = "\0", temp[128] = "\0";
	int i = 0, j = 0, k = 0, m = 0, n = 0, numPrograms = 0;

	while(p1getline(0, buf, 256) != 1) {
		j = k = 0;

		buf[p1strlen(buf) - 1] = '\0';

		j = p1getword(buf, j, word);
		program[i] = p1strdup(word);
		memset(word, 0, 64);

		while( (k = p1getword(buf, j, word)) != -1 ) {
			word[p1strlen(word)] = ' ';

			for(n = 0; n <= k - j - 1; ++n, ++m) {
				temp[m] = word[n];
			}

			memset(word, 0, 64);
			j = k;
		}

		args[i] = p1strdup(temp);

		++m;
		++i;
		memset(buf, 0, 1024);
	}

	numPrograms = i;

	sigset_t set; 
	int sig;
	pid_t mypid;
	sigemptyset(&set);
	sigaddset(&set, SIGUSR1);

	for(i = 0; i < numPrograms; ++i) {
		mypid = fork();

		if(mypid < 0) {
			exit(1);
		}

		if(mypid == 0) { // child

			sigwait(&set, &sig); 

			execvp(program[i], (char* const*)args[i]); 					 

			exit(0);

		} else { // parent
			pid[i] = mypid;
		}

	}

	sleep(2);
	
	for(i = 0; i < numPrograms; ++i) {
		kill(pid[i], SIGUSR1);
		sleep(2);
		kill(pid[i], SIGSTOP);
		sleep(2);
		kill(pid[i], SIGCONT);
	}

	for(i = 0; i < numPrograms; ++i)
		wait(NULL);

	return 0;
}

