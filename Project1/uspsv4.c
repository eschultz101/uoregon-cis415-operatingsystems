// Eric Schultz
// CIS 415
// Project 1: USPS Version 4
// 5/2/15

#include "p1fxns.h"
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

#define PROCESS_TIME 3

void alarm_handler (int sig) {

	pid_t pid;
	int status;

	// suspend running child process (SIGSTOP)
	while ((pid = waitpid(0, &status, WNOHANG)) > 0) {
		if(WIFCONTINUED(status)) {
			kill(pid, SIGSTOP);
		}
	}

	// select and execute waiting process (SIGCONT)
	while ((pid = waitpid(-1, &status, WNOHANG)) > 0) {
		if(WIFSTOPPED(status)) {
			kill(pid, SIGCONT);
		}
	}

	// reset alarm
	alarm(PROCESS_TIME);
}


int main(void) {

	int pid[128] = { 0 };
	char* program[128] = { "\0" };
	char* args[128] = { "\0" };
	char buf[1024] = "\0", word[64] = "\0", temp[128] = "\0";
	int i = 0, j = 0, k = 0, m = 0, n = 0, numPrograms = 0;

	while(p1getline(0, buf, 256) != 1) {
		j = k = 0;

		buf[p1strlen(buf) - 1] = '\0';

		j = p1getword(buf, j, word);
		program[i] = p1strdup(word);
		memset(word, 0, 64);

		while( (k = p1getword(buf, j, word)) != -1 ) {
			word[p1strlen(word)] = ' ';

			for(n = 0; n <= k - j - 1; ++n, ++m) {
				temp[m] = word[n];
			}

			memset(word, 0, 64);
			j = k;
		}

		args[i] = p1strdup(temp);

		++m;
		++i;
		memset(buf, 0, 1024);
	}

	numPrograms = i;

	sigset_t set;
	sigemptyset(&set);
	sigaddset(&set, SIGALRM);
	signal(SIGALRM, alarm_handler);

	pid_t mypid;
	for(i = 0; i < numPrograms; ++i) {
		mypid = fork();

		if(mypid < 0) {
			exit(1);
		}

		if(mypid == 0) { // child

			execvp(program[i], (char* const*)args[i]); 					 

			exit(0);

		} else { // parent
			pid[i] = mypid;
		}

	}

	for(i = 0; i < numPrograms; ++i) {
		kill(pid[i], SIGSTOP);
		alarm(PROCESS_TIME);
	}
	
	for(i = 0; i < numPrograms; ++i)
		wait(NULL); // no longer good enough?

	return 0;
}

