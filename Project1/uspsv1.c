// Eric Schultz
// CIS 415
// Project 1: USPS Version 1
// 4/28/15
// This is my own work.

#include "p1fxns.h"
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>

int main(void) {

	int pid[128] = { 0 }, numPrograms = 0;
	char* program[128] = { "\0" };
	char* args[128] = { "\0" };
	char buf[1024] = "\0", word[64] = "\0", temp[128] = "\0";
	int i = 0, j = 0, k = 0, m = 0, n = 0, status;

	while(p1getline(0, buf, 256) != 1) {
		j = k = 0;

		buf[p1strlen(buf) - 1] = '\0';

		j = p1getword(buf, j, word);
		program[i] = p1strdup(word);
		memset(word, 0, 64);

		while( (k = p1getword(buf, j, word)) != -1 ) {
			word[p1strlen(word)] = ' ';

			for(n = 0; n <= k - j - 1; ++n, ++m) {
				temp[m] = word[n];
			}

			memset(word, 0, 64);
			j = k;
		}

		args[i] = p1strdup(temp);

		++m;
		++i;
		memset(buf, 0, 1024);
	}

	numPrograms = i;
	
	for(i = 0; i < numPrograms; ++i) {
		pid[i] = fork();
		if(pid[i] == 0) {
			execvp(program[i], (char* const*)args[i]);
		}
	}

	for(i = 0; i < numPrograms; ++i) {
		waitpid(pid[i], &status, WNOHANG);
	}

	return 0;
}
