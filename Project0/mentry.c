#include "mentry.h"
#include <stdlib.h>
#include <string.h>

MEntry* me_get(FILE* fd) {

	MEntry* me = (MEntry*)malloc(sizeof(MEntry));
	char temp[50] = "\0";
	char temp2[50] = "\0";
	int t = 0, len = 0;

	// this block takes care of first line in 3-line input set
	if( (fscanf(fd, "%[^,]%*c%*c%[^\n]%*c", &temp, &temp2)) == EOF ) return NULL;
	len = strlen(temp);
	if( !(me->surname = (char*)malloc((len + 1) * sizeof(char))) ) return NULL;
	memset(me->surname, 0, len + 1);
	strcpy(me->surname, temp);
	memset(temp, 0, len);
	len = strlen(temp2);
	if( !(me->firstName = (char*)malloc((len + 1) * sizeof(char))) )  return NULL;
	memset(me->firstName, 0, len + 1);
	strcpy(me->firstName, temp2);
	memset(temp2, 0, len);
	len = 0;

	// takes care of second line in 3-line set
	if(fscanf(fd, "%d%*c", &t) != 1) {
		me->house_number = 0;
	} else {
		me->house_number = t;
	}
	fscanf(fd, "%s", &temp);
	len = strlen(temp);

	// third line
	fscanf(fd, "%[^1234567890]", &temp2);
	len += strlen(temp2);
	if( !(me->full_address = (char*)malloc((len + 1) * sizeof(char))) ) return NULL;
	memset(me->full_address, 0, len + 1);
	strcpy(me->full_address, temp); 
	strcat(me->full_address, temp2);
	memset(temp, 0, len);
	fscanf(fd, "%[^\n]%*c", &temp);
	if( !(me->zipcode = (char*)malloc(6 * sizeof(char))) ) return NULL;
	memset(me->zipcode, 0, 6);
	strcpy(me->zipcode, temp);

	return me;
}


unsigned long me_hash(MEntry* me, unsigned long size) {

	unsigned long hk = 0; 
	int len = 0;

	len = strlen(me->surname);
	for(int i = 0; i < len; ++i) {
		hk += me->surname[i];
	}
	hk *= atoi(me->zipcode);
	hk += me->house_number;

	hk = hk % size;

	return hk;
}


void me_print(MEntry* me, FILE* fd) {

	// print full address on fd
	if(me->house_number == 0) {
		fprintf(fd, "%s, %s\n%s%s\n", me->surname, me->firstName, me->full_address, me->zipcode);
	} else {
		fprintf(fd, "%s, %s\n%d %s%s\n", me->surname, me->firstName, me->house_number, 
			me->full_address, me->zipcode);
	}

}


int me_compare(MEntry* me1, MEntry* me2) {

	if(strcmp(me1->surname, me2->surname) == 0) {
		if(me1->house_number == me2->house_number) {
			if(strcmp(me1->zipcode, me2->zipcode) == 0) {
				return 0;
			}
		}
	}
	return -1;
}


void me_destroy(MEntry* me) {

	// deallocate surname, zipcode, and full_address as well as the MEntry itself (check for memory leaks)


}
