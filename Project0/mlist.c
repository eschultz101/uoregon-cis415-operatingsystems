#include "mlist.h"
#include <stdio.h>
#include <stdlib.h>

#define MLIST_SIZE 10240
#define BUCKET_SIZE 320

MList* ml_create(void) {

	MList* ml = (MList*)malloc(sizeof(MList));

	ml->me = (MEntry**)malloc(MLIST_SIZE * sizeof(MEntry*));

	for(int i = 0; i < MLIST_SIZE; ++i) {
		ml->me[i] = (MEntry*)malloc(BUCKET_SIZE * sizeof(MEntry));
	}

	return ml;
}

int ml_add(MList** ml, MEntry* me) {
	
	unsigned long hk = me_hash(me, MLIST_SIZE);
	int i = 0;

	// if bucket has something in it already, add to next available slot
	while((*ml)->me[hk][i].surname) {
		if(ml_lookup(*ml, me)) {
			return 1;
		}
		if(i == BUCKET_SIZE) {
			return 0;
		}
		i++;
	}

	(*ml)->me[hk][i] = *me;
	
	return 1;

}

// looks for MEntry that could be a duplicate of me, return it if found else NULL
MEntry* ml_lookup(MList* ml, MEntry* me) {

	unsigned long hk = me_hash(me, MLIST_SIZE);
	int i = 0;

	while(ml->me[hk][i].surname) {
		if(me_compare(me, &(ml->me[hk][i])) == 0) {
			return &(ml->me[hk][i]);
		}
		++i;
	}

	return NULL;
}


void ml_destroy(MList* ml) {

	for(int i = 0; i < MLIST_SIZE; ++i) {
		for(int j = 0; j < BUCKET_SIZE; ++j) {
				
		}
	} 

}
