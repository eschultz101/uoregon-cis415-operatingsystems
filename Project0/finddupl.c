#include <stdio.h>
#include "mentry.h"
#include "mlist.h"

static void usage(void) {
	fprintf(stderr, "usage: finddupl\n");
}

int main(int argc, char* argv[]) {
	MEntry* mep, *meq;
	MList* ml;
	FILE* fpIn = fopen(argv[1], "r");

	if (argc > 2) {
		usage(); return -1;
	}
		
	ml = ml_create();
	int i = 1;
	while ((mep = me_get(fpIn)) != NULL) {
		meq = ml_lookup(ml, mep);
		if (meq == NULL)
			(void) ml_add(&ml, mep);
		else {
			printf("Potential duplicate\n");
			printf("===================\n");
			me_print(mep, stdout);
			printf("==========\n");
			me_print(meq, stdout);
			printf("\n");
		}
		++i;
	}

	ml_destroy(ml);

	return 0;
}
